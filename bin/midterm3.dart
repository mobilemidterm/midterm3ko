import "dart:io";

List convert(String expression, var regExp){
  return expression.split(regExp);
}







bool isOperatorSign(String o) {
  return o == '+' ||
    o == '-' ||
    o == '*' ||
    o == '/';
}

bool isValidInfixSyntax(String o){
  return o == '+' ||
    o == '-' ||
    o == '*' ||
    o == '/' ||
    double.tryParse(o) != null;
}

int precedence(String o){
  return (o == '+' || o == '-') ? 1 :
  (o == '*' || o == '/') ? 2 : 0;
}

List infix2Postfix(List infixExpresstion){
  var expression = infixExpresstion;
  var operators = [];
  var postfix = [];
  
  for(int i=0; i < expression.length; i++){
    var token = expression[i];
    if(!isValidInfixSyntax(token))continue;
    if(double.tryParse(token) != null){
      postfix.add(double.tryParse(token));
    }else if(isOperatorSign(token)){
      while(operators.isNotEmpty && operators.last != '(' && precedence(token) < precedence(operators.last)){
        postfix.add(operators.removeLast());
      }
      operators.add(token);
    }else if(token == '('){
      operators.add(token);
    }else if(token == ')'){
      while(operators.last != '('){
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  }
  
  while(operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  
  return postfix;
}




num evaluatePosfix(var posfixExpression) {
  var values =[];
  var token = posfixExpression;
  var tokenlist = [];
  
  if(token is List) {
    for(var char in token){
      tokenlist.add(char.toString());
    }
  }else{
    tokenlist = token.split(' ');
  }
  
  for (var char in tokenlist) {
    if (double.tryParse(char) != null) {
      values.add(double.tryParse(char));

    }else{
      try{
        num right = values.removeLast();
        num left = values.removeLast();
        num result = 0;
        if (char == '+') {
          result = left+right;
        }else if(char == '-'){
          result = left-right;
        }else if(char == '*'){
          result = left*right;
        }else if(char == '/'){
          result = left/right;
        }
        values.add(result);
      }catch(e){
        print("According to my calculations, the postfix expression $posfixExpression evaluates to NaN.");
        return 0;
      }
    }
  }
    return values[0];
}
void main() {

  
  //1
  print("Token: ");
  var infixExpressionInput = stdin.readLineSync()!;


  var regExp = ' ';

  var result = convert(infixExpressionInput, regExp);
  print("Result: $result");

  print(infix2Postfix(result));


  //3
  
  print(evaluatePosfix(infix2Postfix(result)));


}